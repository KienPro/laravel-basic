<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

    <div class="col-md-6">
        <input type="text" class="form-control" name="name" value="{{ $data->name ?? '' }}" id="name" placeholder="Enter product name">
        @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">Price</label>

    <div class="col-md-6">
        <input type="number" class="form-control" name="price" value="{{ $data->price ?? ''}}" id="price" placeholder="$" step="0.01">
        @if ($errors->has('price'))
            <span class="text-danger">{{ $errors->first('price') }}</span>
        @endif
    </div>
</div>