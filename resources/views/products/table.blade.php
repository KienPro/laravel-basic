<div style="overflow-x: auto">
    <table class="table table-hover">
        <thead>
            <tr>
                <th style="text-align:center">ID</th>
                <th>Product Name</th>
                <th>Price</th>
                <th class="text-center" style="width: 160px"><div style="width: 100px"></div> Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($products as $index => $list)
            <tr>
                <td style="text-align:center">{{ $products ->firstItem() + $index }}</td>
                <td>{{ $list->name }}</td>
                <td>{{ $list->price }}</td>
                <td class="text-center">
                    <a href="{{ route('products.show', $list->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete-{{ $list->id }}"><i class="fa fa-trash"></i> Delete</button>
                    @include('products.modal-delete')
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="99" class="text-center">
                        No Product Found!
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>