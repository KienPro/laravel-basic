@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Product Information</h3>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('products.store') }}" method="POST" id="createProduct">
                        @csrf
                        @include('products.form')
                    </form>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <a href="{{ route('products.index') }}" class="btn btn-secondary btn-sm"><i class="fa fa-share fa-flip-horizontal fa-fw"></i>Back</a>
                        <button type="submit" class="btn btn-primary btn-sm" form="createProduct"><i class="fa fa-floppy-o fa-fw"></i>Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection