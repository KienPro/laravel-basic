@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <div class="pull-left">
                            <h3 class="card-title">Laravel</h3>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw"></i>Add new</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <form>
                        <div class="col-md-4 offset-md-8">
                            <div class="input-group mb-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request('search')}}" title="Resfresh">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><a href="{{ route('products.index') }}"><i class="fa fa-refresh"></i></a></div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </form>
                    
                   @include('products.table')
                   <div class="pull-left">
                       Total: {{$products->total()}} records
                   </div>
                   <div class="pull-right">
                        {{ $products -> appends([
                            'search' => request('search'),
                            ])->links() }}
                   </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
