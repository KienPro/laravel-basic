<div class="modal fade" id="modal-delete-{{ $list->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fa fa-trash fa-fw"></i> Deleting Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure, you want to delete product id - ({{ $list->id }})?
        </div>
        <div class="modal-footer">
            <form action="{{ route('products.destroy', $list->id) }}" method="POST">
                @csrf
                @method('delete')
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
      </div>
    </div>
  </div>